"use strict"
// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

function division (a, b) {
    if (b === 0) {
        return "Деление на 0 не возможно!";
    }
    return a/b;
}
console.log(division(10,3));

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

// Технічні вимоги:
// - Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число

// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').

// - Створити функцію, в яку передати два значення та операцію.

// - Вивести у консоль результат виконання функції.




        let numberOne = prompt("Введите первое число");
        while (isNaN(numberOne) || numberOne === "0") {
            alert("Введите число которое не ровняется 0!");
            numberOne = prompt("Введите первое число");
        }
        let simbol = prompt("Введите математический символ (*, /, -, +)");
        while (simbol !== "+" && simbol !== "-" && simbol !== "*" && simbol !== "/") {
            alert("Такой операции не существует!");
            simbol = prompt("Введите математический символ (*, /, -, +)");
        }
        let numberTwo = prompt("Введите второе число");
        while (isNaN(numberTwo) || numberTwo === "0") {
            alert("Введите число которое не ровняется 0!");
            numberTwo = prompt("Введите второе число");
        }
        function calc() {
        if (simbol === "/") {
            return console.log(`${numberOne} / ${numberTwo} = ` + (Number(numberOne) / Number(numberTwo)));
        } else if (simbol === "*") {
            return console.log(`${numberOne} * ${numberTwo} = ` + (Number(numberOne) * Number(numberTwo)));
        } else if (simbol === "+") {
            return console.log(`${numberOne} + ${numberTwo} = ` + (Number(numberOne) + Number(numberTwo)));
        } else if (simbol === "-"){
            return console.log(`${numberOne} - ${numberTwo} = ` + (Number(numberOne) - Number(numberTwo)));
        } else {
            alert("Произошла ошибка!");
        }
    };
    calc(numberOne, numberTwo);

// 3. Опціонально. Завдання:

// Реалізувати функцію підрахунку факторіалу числа.

// Технічні вимоги:

// - Отримати за допомогою модального вікна браузера число, яке введе користувач.

// - За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.

// - Використовувати синтаксис ES6 для роботи зі змінними та функціями.
    
let yourNumber = prompt("Введите любое натуральное число");
function factorial () {
    let number = 1;
    for (let i = 1; i <= yourNumber; i++) {
        number *= i;
    }
    return console.log(`Факториал числа ${yourNumber} = ` + (number));
}
factorial(yourNumber);

